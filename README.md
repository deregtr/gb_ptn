# README FILE
# Files with this format: co*.dat contain geographical cordinates (Easting and Northing) of stations for their respective public transport network.
# This file contains three columns with the first one providing the unique station indentifier for each station and the second and third columns giving the 
# geographical cordinates in Easting and Northing, respectively.
# Files with this format: edges*.dat contain the edges for their respective public transport network. Each edge is defined by its two unique station (node) identifiers
# which uniquely define each station which can be found on the National Transport Data Repository (NTDR) at the website http://data.gov.uk/dataset/nptdr as described in
# section three in the manuscript.
